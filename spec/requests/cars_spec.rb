require 'rails_helper'

RSpec.describe "Cars", type: :request do

  let(:valid_params) { { car: { title: 'my-next-article', desciption: 'abc' } } }
  let(:invalid_params) { { car: { title: '', desciption: 'abc' } } }
  let(:car) {FactoryBot.create :car}

  it 'expect successful updated' do
    u = FactoryBot.create :user
    patch car_path(car, params: valid_params)
    assert_once_targeted('flash-box')
    expect(response).to redirect_to(cars_path)
    assert_channel_to_me(u, 'flash-box') # on redirect, flash has to be sent by channel

  end

  it 'expect update failed' do
    patch car_path(car, params: invalid_params)
    assert_once_targeted('flash-box', "car-#{car.id}-form")
    assert_stream_response("flash-box") # without redirect, flash can be sent on the default way: stream
  end

  it 'expect save failed && fetch target-id from partial!' do

    patch car_path(car, params: invalid_params)

    assert_stream_response("car-#{car.id}-form"){|r|r.css('div.field_with_errors')}
    # note that the target-id is not declared at controller-action :update!
    # turbo_stream_save has fetched it automatically from the partial, found by attribute data_turbo_target

    assert_stream_response('flash-box')
  end

  it 'expect create failed && render template instead of partial' do

    post cars_path(params: invalid_params)

    assert_stream_response('new-car-form') {|r| r.css('.field_with_errors input[name="car[title]"]') }

    assert_stream_response('flash-box') {|r| r.css('.text-content').inner_html.include?('Car could not be created') }

  end

  it 'destroy car' do
    u = FactoryBot.create :user
    delete car_path(FactoryBot.create :car)
    assert_channel_to_me(u, 'flash-box'){|r|r.css('.text-content').inner_html.include?('Car successfully destroyed')}
  end

end
