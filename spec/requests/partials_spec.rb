require 'rails_helper'

RSpec.describe "Partials", type: :request do

  context 'Channel' do
    it 'argument target_id not given by argument: render to channel should fetch the target_id from the partial' do

      post partials_test_channel_path

      assert_channel_to_all('render-to-box') { |html| html.css('turbo-frame#render-to-box') }

    end

    it 'no target given and no target inside partial should raise error' do
      expect do
        post partials_channel_by_params_path(params: {partial: 'without_target_id'})
      end.to raise_exception(RuntimeError, 'No target specified by arguments and no target found inside the rendered partial')

    end

    it 'target given inside partial should work' do
      post partials_channel_by_params_path(params: {partial: 'with_turbo_frame_tag'})
      assert_channel_to_all('target-id-from-partial')
    end
  end


end