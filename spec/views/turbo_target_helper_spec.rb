require 'rails_helper'

RSpec.describe "test/turbo_target_tag", type: :view do

  it "no args, no controller instance variable" do
    assign(:arg, nil)
    render
    assert_select "turbo-target#test-turbo_target_tag"
  end

end
