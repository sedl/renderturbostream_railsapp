require 'rails_helper'

RSpec.describe "form actions", type: :system do

  it "create successful" do
    visit '/'
    click_link 'New Article'
    title = "my-awesome-article-#{SecureRandom.hex(8)}"
    within("#form") do
      fill_in 'Title', with: title
    end
    click_button 'Save'
    expect(page).to have_content title
    within(".callout.success") do
      expect(page).to have_content 'Article successfully created'
    end
    expect(page).to have_content title
  end

  it "creation failed" do
    visit '/'
    click_link 'New Article'
    title = ""
    within("#form") do
      fill_in 'Title', with: title
    end
    expect(find(:css, 'form')).not_to have_css('.field_with_errors')
    click_button 'Save'
    expect(find(:css, 'form')).to have_css('.field_with_errors')
    within(".callout.alert") do
      expect(page).to have_content 'Article could not be created'
    end
  end

  it 'update success' do
    title = "my-awesome-article-#{SecureRandom.hex(8)}"
    title2 = "my-next-title-#{SecureRandom.hex(8)}"
    a = FactoryBot.create(:article, title: title)
    visit edit_article_path(a)
    expect(page).to have_content("Editing article #{title}")
    within("#form") do
      fill_in 'Title', with: title2
    end
    click_button 'Save'
    within(".callout.success") do
      expect(page).to have_content 'Article successfully updated'
    end
    expect(page).to have_content title2
  end

  it 'update failed' do
    title = "my-awesome-article-#{SecureRandom.hex(8)}"
    title2 = ""
    a = FactoryBot.create(:article, title: title)
    visit edit_article_path(a)
    expect(page).to have_content("Editing article #{title}")
    within("#form") do
      fill_in 'Title', with: title2
    end
    expect(find(:css, 'form')).not_to have_css('.field_with_errors')
    click_button 'Save'
    expect(find(:css, 'form')).to have_css('.field_with_errors')
    within(".callout.alert") do
      expect(page).to have_content 'Article could not be updated'
    end
  end

end