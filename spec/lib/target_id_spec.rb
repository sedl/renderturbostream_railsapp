require 'rails_helper'

RSpec.describe 'Libs', type: :model do

  it 'with controller' do
    libs = RenderTurboStream::Libs
    expect(libs.target_id( 'articles/_form', nil)).to eq('articles-form')
  end

  it 'with model' do
    art = FactoryBot.create(:article)
    libs = RenderTurboStream::Libs
    expect(libs.target_id( 'articles/_form', art)).to eq("article-#{art.id}-form")
  end

  it 'array' do
    art = FactoryBot.create(:article)
    car = FactoryBot.create(:car)
    libs = RenderTurboStream::Libs
    expect(libs.target_id( 'articles/_form', [art, car])).to eq("article-#{art.id}-car-#{car.id}-form")
  end

  it 'array with string' do
    art = FactoryBot.create(:article)
    libs = RenderTurboStream::Libs
    expect(libs.target_id( 'articles/_form', [art, 'hello'])).to eq("article-#{art.id}-hello-form")
  end

  it 'string' do
    libs = RenderTurboStream::Libs
    expect(libs.target_id( 'articles/_form', 'hello')).to eq("hello")
  end

  it 'array with number' do
    libs = RenderTurboStream::Libs
    expect(libs.target_id( 'articles/_form', 33)).to eq("33-form")
  end

  it 'array with number' do
    libs = RenderTurboStream::Libs
    expect(libs.target_id( 'articles/_form', SecureRandom)).to eq("SecureRandom-form")
  end



end
