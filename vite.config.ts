import {defineConfig} from 'vite'
import RubyPlugin from 'vite-plugin-ruby'
import FullReload from 'vite-plugin-full-reload'
import inject from "@rollup/plugin-inject";
import {svelte} from "@sveltejs/vite-plugin-svelte";

export default defineConfig({
    plugins: [
        inject({
            $: 'jquery',
            jQuery: 'jquery',
        }),
        RubyPlugin(),
        FullReload([
            'app/views/**/*',
            // => add paths you want to watch
        ], {delay: 0}),
        svelte({}),
    ],
})
