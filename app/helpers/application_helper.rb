module ApplicationHelper

  # replacements for devise
  def current_user
    if Rails.env.test?
      User.last
    else
      User.find_by(id: session[:user_id])
    end
  end







end
