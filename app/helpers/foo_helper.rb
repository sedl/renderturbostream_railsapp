module FooHelper
  def present_foo(object)
    presenter = FooPresenter.new(object, self)
    yield presenter if block_given?
    presenter
  end
end
