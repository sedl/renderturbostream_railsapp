class ChannelController < ApplicationController

  include FooHelper

  def redirect
    a = 1
  end

  def redirect1

  end

  def redirect2
    turbo_stream_save(
      true,
      if_success_redirect_to: channel_redirect3_path,
      object: Article.new,
      flash_controller_action_name: :create
    )
  end

  def redirect3

  end

  def flash_test
    if params['to_all']
      render_to_all(
        'flash-box',
        :prepend,
        partial: 'layouts/flash',
        locals: { 'message' => 'its working by cable-stream!' }
      )
    else
      render_to_me(
        'flash-box',
        :prepend,
        partial: 'layouts/flash',
        locals: { 'message' => 'its working by cable-stream!' }
      )
    end
    render layout: false
  end

  def make_red
    action_to_all(
      'add_css_class',
      '#colored-element',
      'background-red'
    )
    action_to_all(
      'remove_css_class',
      '#colored-element',
      'background-green'
    )
  end

  def make_red_to_me
    action_to_me(
      'add_css_class',
      '#colored-element',
      'background-red'
    )
    action_to_me(
      'remove_css_class',
      '#colored-element',
      'background-green'
    )
  end

  def make_red_to_authenticated_group
    action_to_authenticated_group(
      helpers.current_user.group,
      'add_css_class',
      '#colored-element',
      'background-red'
    )
    action_to_authenticated_group(
      helpers.current_user.group,
      'remove_css_class',
      '#colored-element',
      'background-green'
    )
  end

  def make_green
    action_to_all(
      'remove_css_class',
      '#colored-element',
      'background-red'
    )
    action_to_all(
      'add_css_class',
      '#colored-element',
      'background-green'
    )
  end

  def partial_to_all
    render_to_all(
      'render-to-box',
      partial: 'channel/cable_easy_partial',
      locals: {
        comment: 'simple partial to all users rendered'
      }
    )
  end

  def template_to_all
    render_to_all(
      'render-to-box',
      locals: {
        comment: 'rendered default template to all visitors'
      }
    )
  end

  def multiple_to_all
    render_to_all(
      'render-to-box',
    )
    render_to_all(
      'flash-box',
      :prepend,
      partial: 'layouts/flash',
      locals: { message: 'flash one from multiple to all', success: true }
    )
  end

  def template_to_me
    render_to_me(
      'render-to-box',
      locals: {
        comment: "rendered default template to me (#{helpers.current_user.name})"
      }
    )
  end

  def template_to_authenticated_group
    render_to_authenticated_group(
      helpers.current_user.group,
      'render-to-box',
      locals: {
        comment: "rendered default template to group «#{helpers.current_user.group}»"
      }
    )
  end

  def cable_template
    render_to_all(
      'template-box',
      'replace',
      locals: { greetings: 'greetings from locals' }
    )
  end

  def class_method
    channel = RenderTurboStream::Channel.new(
      :all,
      partial: 'channel/class_method_partial'
    )
    channel.send

    # for testing
    RenderTurboStream::Test::Request::Libs.set_test_responses(response, channel.test_responses)
  end

end