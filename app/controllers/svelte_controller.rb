class SvelteController < ApplicationController
  def index

  end

  def push
    render layout: false, formats: [:turbo_stream]
  end

  def push2
    render_to_all(
      'svelte-box',
      partial: 'svelte/push2',
    )
  end

end