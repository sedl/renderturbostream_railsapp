class TurboPowerController < ApplicationController
  def index
  end

  def action
    ary = JSON.parse(params['ary'])
    render_turbo_stream(
      ary
    )
  end

end